const Speech = require('@google-cloud/speech');
const projectId = 'magic-wand';
const speechClient = Speech({
  projectId: projectId
});
const fileName = './resources/test.wav';
const options = {
  encoding: 'LINEAR16',
  sampleRate: 8000,
  speechContext: {
	  phrases:["incendio", "silencio", "accio", "alohomora", "scourgify", "sonorous", "quietus", "lumos", "nox"]
	}
};

const settings = {
  ip: "10.190.78.136",
  port: 3443
};

const fs = require("fs");
var transcription;
var chunkNum;

const net = require('net');

function startClient(){
	const client = net.connect(settings.port, settings.ip, function () {
	  	const outStream = fs.createWriteStream(fileName);
		chunkNum = 0;
		client.on("data", function (data) {
		    try {
		      outStream.write(data);
		      chunkNum++;
		    }
		    catch (ex) {
		        console.error("Er!" + ex);
		    }
		});
		client.on("end", function(){
			outStream.end();
			getTranscription();
			console.log(`contained ${chunkNum} chunks`);
			startClient();
		});
		client.on("error", function(){
			startClient();
		})
	});
}

startClient();

function castSpell(spell){
	console.log(`casting ${spell}`);
	if(spell == 'lumos'){
		lumos();
	}
	if(spell == 'nox'){
		nox();
	}
}

function getTranscription(){
	console.log('transcribing');
	speechClient.recognize(fileName, options)
	  .then((results) => {
	    transcription = results[0];
	    if (options.speechContext.phrases.includes(transcription)){
		  	console.log(`about to cast: ${transcription}`);
	    	castSpell(transcription);	    	
	    }
	    else {
	    	console.log(`Spell not recognized: ${transcription}`);	    	
	    }
	  }, (err) => {
	  	console.log(`ERROR: ${err}`);
	  });
}

var WeMo = new require('wemo')
var wemoSwitch = new WeMo('10.190.72.45', 49153);

function lumos(){
	wemoSwitch.setBinaryState(1, function(err, result) { 
	    if (err) console.error(err);
	});
}

function nox(){
	wemoSwitch.setBinaryState(0, function(err, result) { 
	    if (err) console.error(err);
	});
}


