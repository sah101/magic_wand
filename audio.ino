#define RECORDING_LENGTH 10
#define MICROPHONE_PIN A5
#define AUDIO_BUFFER_MAX 8192

#define RED_PIN D3
#define BLUE_PIN D4
#define GREEN_PIN D5

#include <application.h>
#include <spark_wiring_i2c.h>

// ADXL345 I2C address is 0x53(83)
#define Addr 0x53

int xAccl = 0, yAccl =  0, zAccl = 0;
int prevX = 0, prevY = 0, prevZ = 0;

int audioStartIdx = 0, audioEndIdx = 0;
bool moving = false;
uint16_t audioBuffer[AUDIO_BUFFER_MAX];
uint16_t txBuffer[AUDIO_BUFFER_MAX];

// version without timers
unsigned long lastRead = micros();
char myIpAddress[24];

TCPClient audioClient;
TCPClient checkClient;
TCPServer audioServer = TCPServer(3443);

void setup() {
    
    //mic setup
    Serial.begin(115200);
    pinMode(MICROPHONE_PIN, INPUT);
    pinMode(RED_PIN, OUTPUT);
    pinMode(BLUE_PIN, OUTPUT);
    pinMode(GREEN_PIN, OUTPUT);
    
    digitalWrite(RED_PIN, HIGH);
    digitalWrite(BLUE_PIN, HIGH);
    digitalWrite(GREEN_PIN, HIGH);

    digitalWrite(RED_PIN, LOW);
    delay(500);
    digitalWrite(RED_PIN, HIGH);
    delay(500);
    digitalWrite(BLUE_PIN, LOW);
    delay(500);
    digitalWrite(BLUE_PIN, HIGH);
    delay(500);
    digitalWrite(GREEN_PIN, LOW);
    delay(500);
    digitalWrite(GREEN_PIN, HIGH);


    Spark.variable("ipAddress", myIpAddress, STRING);
    IPAddress myIp = WiFi.localIP();
    sprintf(myIpAddress, "%d.%d.%d.%d", myIp[0], myIp[1], myIp[2], myIp[3]);
    
    
    // 1/8000th of a second is 125 microseconds
    audioServer.begin();
    
    lastRead = micros();
    
    //accelerometer setup
    // Set variable
      Particle.variable("i2cdevice","ADXL345");
      Particle.variable("xAccl",xAccl);
      Particle.variable("yAccl",yAccl);
      Particle.variable("zAccl",zAccl);
    
      // Initialise I2C communication as MASTER
      Wire.begin();
      // Initialise serial communication, set baud rate = 9600
      Serial.begin(9600);
    
      // Start I2C transmission
      Wire.beginTransmission(Addr);
      // Select bandwidth rate register
      Wire.write(0x2C);
      // Select output data rate = 100 Hz
      Wire.write(0x0A);
      // Stop I2C Transmission
      Wire.endTransmission();
    
      // Start I2C transmission
      Wire.beginTransmission(Addr);
      // Select power control register
      Wire.write(0x2D);
      // Select auto sleep disable
      Wire.write(0x08);
      // Stop I2C transmission
      Wire.endTransmission();
    
      // Start I2C transmission
      Wire.beginTransmission(Addr);
      // Select data format register
      Wire.write(0x31);
      // Select full resolution, +/-2g
      Wire.write(0x08);
      // End I2C transmission
      Wire.endTransmission();
      delay(300);
}

void loop() {
    checkClient = audioServer.available();
    if (checkClient.connected()) {
        audioClient = checkClient; 
    }
    
    unsigned int data[6];
  for(int i = 0; i < 6; i++)
  {
    // Start I2C transmission
    Wire.beginTransmission(Addr);
    // Select data register
    Wire.write((50+i));
    // Stop I2C transmission
    Wire.endTransmission();

    // Request 1 byte of data from the device
    Wire.requestFrom(Addr,1);
    // Read 6 bytes of data
    // xAccl lsb, xAccl msb, yAccl lsb, yAccl msb, zAccl lsb, zAccl msb
    if(Wire.available()==1)
    {
      data[i] = Wire.read();
    }
  delay(100);
  }

  // Convert the data to 10-bits
  int xAccl = (((data[1] & 0x03) * 256) + data[0]);
  if(xAccl > 511)
  {
    xAccl -= 1024;
  }
  int yAccl = (((data[3] & 0x03) * 256) + data[2]);
  if(yAccl > 511)
  {
    yAccl -= 1024;
  }
  int zAccl = (((data[5] & 0x03) * 256) + data[4]);
  if(zAccl > 511)
  {
    zAccl -= 1024;
  }
  
    if (abs(xAccl) > 300 || abs(yAccl) > 300 || abs(zAccl) > 300){
        moving = true;
    } else {
        moving = false;
    }
    
    
    //listen for 100ms, taking a sample every 125us,
    //and then send that chunk over the network.
    if(moving){
        digitalWrite(GREEN_PIN, LOW);
        Serial.print("\nrecording\n");
        for(int i=0;i<RECORDING_LENGTH;i++){
            listenAndSend(100);
        }
        digitalWrite(GREEN_PIN, HIGH);
        Serial.print("\ndone\n");
        audioClient.stop();
    }
}

void listenAndSend(int delay) {
    unsigned long startedListening = millis();
    
    while ((millis() - startedListening) < delay) {
        unsigned long time = micros();
        
        if (lastRead > time) {
            // time wrapped?
            //lets just skip a beat for now, whatever.
            lastRead = time;
        }
        
        //125 microseconds is 1/8000th of a second
        if ((time - lastRead) > 125) {
            lastRead = time;
            readMic();
        }
    }
    sendAudio();
}

 
// Callback for Timer 1
void readMic(void) {
    uint16_t value = analogRead(MICROPHONE_PIN);
    if (audioEndIdx >= AUDIO_BUFFER_MAX) {
        audioEndIdx = 0;
    }
    audioBuffer[audioEndIdx++] = value;
}

void copyAudio(uint16_t *bufferPtr) {
    //if end is after start, read from start->end
    //if end is before start, then we wrapped, read from start->max, 0->end
    
    int endSnapshotIdx = audioEndIdx;
    bool wrapped = endSnapshotIdx < audioStartIdx;
    int endIdx = (wrapped) ? AUDIO_BUFFER_MAX : endSnapshotIdx;
    int c = 0;
    
    for(int i=audioStartIdx;i<endIdx;i++) {
        // do a thing
        bufferPtr[c++] = audioBuffer[i];
    }
    
    if (wrapped) {
        //we have extra
        for(int i=0;i<endSnapshotIdx;i++) {
            // do more of a thing.
            bufferPtr[c++] = audioBuffer[i];
        }
    }
    
    //and we're done.
    audioStartIdx = audioEndIdx;
    
    if (c < AUDIO_BUFFER_MAX) {
        bufferPtr[c] = -1;
    }
}

// Callback for Timer 1
void sendAudio(void) {
    copyAudio(txBuffer);
    
    int i=0;
    uint16_t val = 0;
        
    if (audioClient.connected()) {
       write_socket(audioClient, txBuffer);
    }
    else {
        while( (val = txBuffer[i++]) < 65535 ) {
            Serial.print(val);
            Serial.print(',');
        }
        Serial.println("DONE");
    }
}


// an audio sample is 16bit, we need to convert it to bytes for sending over the network
void write_socket(TCPClient socket, uint16_t *buffer) {
    int i=0;
    uint16_t val = 0;
    
    int tcpIdx = 0;
    uint8_t tcpBuffer[1024];
    
    while( (val = buffer[i++]) < 65535 ) {
        if ((tcpIdx+1) >= 1024) {
            socket.write(tcpBuffer, tcpIdx);
            tcpIdx = 0;
        }
        
        tcpBuffer[tcpIdx] = val & 0xff;
        tcpBuffer[tcpIdx+1] = (val >> 8);
        tcpIdx += 2;
    }
    
    // any leftovers?
    if (tcpIdx > 0) {
        socket.write(tcpBuffer, tcpIdx);
    }
}
    
